;;;; -*- lisp -*-

(defpackage :parse-html.system
  (:use :common-lisp
        :asdf))

(in-package :parse-html.system)

(defsystem :parse-html
  :components ((:file "if" :depends-on ("packages"))
               (:file "packages")
               (:file "phtml" :depends-on ("if" "packages"))
               (:file "sanitize" :depends-on ("packages" "write-lhtml" "phtml"))
               (:file "write-lhtml" :depends-on ("packages"))))
