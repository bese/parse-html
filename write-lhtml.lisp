;; -*- lisp -*-

(in-package :parse-html)

(defun write-lhtml (lhtml stream)
  (walk-lhtml lhtml
              (lambda (tag-name attributes body)
                (write-char #\< stream)
                (write-string (string-downcase tag-name) stream)
                (when attributes
                  (loop
                     for (key value) on attributes by #'cddr
                     for quote = (if (position #\' value)
                                     #\"
                                     #\')
                     do (write-char #\Space stream)
                     do (write-string (string-downcase key) stream)
                     do (write-char #\= stream)
                     do (write-char quote stream)
                     do (write-string value stream)
                     do (write-char quote stream)))
                (if body
                    (progn
                      (write-char #\> stream)
                      (dolist (b body) (write-lhtml b stream))
                      (write-char #\< stream)
                      (write-string (string-downcase tag-name) stream)
                      (write-char #\> stream))
                    (progn
                      (write-string "/>" stream))))
              (lambda (string)
                (write-string string stream))))
